<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.com.mangroo</groupId>
    <artifactId>temperature-service</artifactId>
    <version>1.2-SNAPSHOT</version>
    <packaging>jar</packaging>

    <properties>
        <cucumber-version>6.2.2</cucumber-version>
        <java.version>11</java.version>
        <unit-tests.skip>false</unit-tests.skip>
        <integration-tests.skip>false</integration-tests.skip>
    </properties>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.3.1.RELEASE</version>
    </parent>

    <profiles>
        <!-- The Configuration of the unit profile. This is the default profile -->
        <profile>
            <id>unit-test</id>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <properties>
                <build.profile.id>unit-test</build.profile.id>
                <skip.integration.tests>true</skip.integration.tests>
                <skip.unit.tests>false</skip.unit.tests>
                <skip.startlocaldynamo>true</skip.startlocaldynamo>
            </properties>
        </profile>
        <!-- The Configuration of the integration-test profile -->
        <profile>
            <id>integration-test</id>
            <properties>
                <build.profile.id>integration-test</build.profile.id>
                <skip.integration.tests>false</skip.integration.tests>
                <skip.unit.tests>true</skip.unit.tests>
                <skip.startlocaldynamo>false</skip.startlocaldynamo>
            </properties>
        </profile>
    </profiles>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
        </dependency>
        <dependency>
            <groupId>com.github.derjust</groupId>
            <artifactId>spring-data-dynamodb</artifactId>
            <version>5.1.0</version>
        </dependency>
        <!-- Cucumber-->
        <dependency>
            <groupId>io.cucumber</groupId>
            <artifactId>cucumber-java</artifactId>
            <version>${cucumber-version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>io.cucumber</groupId>
            <artifactId>cucumber-junit</artifactId>
            <version>${cucumber-version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>io.cucumber</groupId>
            <artifactId>cucumber-spring</artifactId>
            <version>${cucumber-version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-configuration-processor</artifactId>
            <optional>true</optional>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>

            <!-- Run *Test.java tests as unit tests during test phase  -->
            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>3.0.0-M3</version>
                <configuration>
                    <skipTests>${skip.unit.tests}</skipTests>
                </configuration>
            </plugin>

            <!-- Run *IT.java tests as integration tests during verify phase -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-failsafe-plugin</artifactId>
                <version>3.0.0-M3</version>
                <configuration>
                    <skipTests>${skip.integration.tests}</skipTests>
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>integration-test</goal>
                            <goal>verify</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>io.fabric8</groupId>
                <artifactId>docker-maven-plugin</artifactId>
                <extensions>true</extensions>
                <configuration>
                    <images>
                        <image>
                            <alias>dynamodb</alias>
                            <name>amazon/dynamodb-local</name>
                            <run>
                                <wait>
                                    <time>10000</time>
                                </wait>
                                <env>

                                </env>
                                <ports>
                                    <port>8000:8000</port>
                                </ports>
                                <volumes>
                                    <bind>

                                    </bind>
                                </volumes>
                            </run>
                        </image>
                    </images>
                    <skip>${skip.startlocaldynamo}</skip>
                </configuration>
                <executions>
                    <execution>
                        <id>docker:start</id>
                        <phase>pre-integration-test</phase>
                        <goals>
                            <goal>start</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>docker:stop</id>
                        <phase>post-integration-test</phase>
                        <goals>
                            <goal>stop</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>net.masterthought</groupId>
                <artifactId>maven-cucumber-reporting</artifactId>
                <version>3.15.0</version>
                <executions>
                    <execution>
                        <id>execution</id>
                        <phase>post-integration-test</phase>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                        <configuration>
                            <projectName>automation-demo</projectName>
                            <outputDirectory>${project.build.directory}/cucumber-reports</outputDirectory>
                            <jsonFiles>
                                <param>${project.build.directory}/cucumber-report.json</param>
                            </jsonFiles>
<!--                            <cucumberOutput>${project.build.directory}/temperatureReading.json</cucumberOutput>-->
                            <!--<skippedFails>true</skippedFails>-->
                            <!--<enableFlashCharts>false</enableFlashCharts>-->
                            <!--<buildNumber>${build_number}</buildNumber>-->
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>


</project>